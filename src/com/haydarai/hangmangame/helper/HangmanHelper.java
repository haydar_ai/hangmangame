package com.haydarai.hangmangame.helper;

/**
 * HangmanHelper.java Purpose: Class with method that will help in generating
 * Hangman game
 * 
 * @author Haydar Ali Ismail
 * @version 1.0
 */

public class HangmanHelper {
	/**
	 * Print hangman picture
	 * 
	 * @param badGuesses
	 */
	public static void printHangman(int badGuesses) {
		int poleLines = 6;
		System.out.println("  ____");
		System.out.println("  |  |");

		if (badGuesses == 7) {
			System.out.println("  |  |");
			System.out.println("  |  |");
		}

		if (badGuesses > 0) {
			System.out.println("  |  O");
			poleLines = 5;
		}

		if (badGuesses > 1) {
			poleLines = 4;
			if (badGuesses == 2) {
				System.out.println("  |  |");
			} else if (badGuesses == 3) {
				System.out.println("  | \\|");
			} else if (badGuesses >= 4) {
				System.out.println("  | \\|/");
			}
		}

		if (badGuesses > 4) {
			poleLines = 3;
			if (badGuesses == 5) {
				System.out.println("  | /");
			} else if (badGuesses >= 6) {
				System.out.println("  | / \\");
			}
		}

		if (badGuesses == 7) {
			poleLines = 1;
		}

		for (int k = 0; k < poleLines; k++) {
			System.out.println("  |");
		}
		System.out.println("__|__");
		System.out.println();
	}
}
