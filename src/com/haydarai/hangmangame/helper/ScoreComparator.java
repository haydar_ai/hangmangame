package com.haydarai.hangmangame.helper;

import java.util.Comparator;

import com.haydarai.hangmangame.model.Score;

/**
 * ScoreComparator.java Purpose: compare score for sorting arraylist
 * 
 * @author Haydar Ali Ismail
 * @version 1.0
 */

public class ScoreComparator implements Comparator<Score> {

	@Override
	public int compare(Score s1, Score s2) {
		if (s1.getScore() < s2.getScore()) {
			return 1;
		} else {
			return -1;
		}
	}

}
