package com.haydarai.hangmangame.helper;

/**
 * StringHelper.java Purpose: Class with method that will help in manipulating
 * String
 * 
 * @author Haydar Ali Ismail
 * @version 1.0
 */

public class StringHelper {

	/**
	 * Convert word into underscore with the same length
	 * 
	 * @param word
	 * @return underscores
	 */
	public static String convertStringToUnderscore(String word) {
		String underscores = new String();
		for (int i = 0; i < word.length(); i++) {
			underscores += "_";
		}
		return underscores;
	}
}
