package com.haydarai.hangmangame.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import com.haydarai.hangmangame.model.Score;

/**
 * ScoreHelper.java Purpose: Class with method that will help in saving and
 * loading scores saved
 * 
 * @author Haydar Ali Ismail
 * @version 1.0
 */

public class ScoreHelper {

	private static Scanner sc = new Scanner(System.in);

	/**
	 * Method to input score to an arraylist of scores
	 * 
	 * @param arraylist
	 *            of scores
	 * @param remaining
	 *            life
	 */
	public static void inputScore(ArrayList<Score> scores, int remainingLife) {
		System.out.print("Input your name: ");
		Score score = new Score(sc.next(), remainingLife);
		scores.add(score);
		Collections.sort(scores, new ScoreComparator());
	}

	/**
	 * Method to load scores from an arraylist of scores
	 * 
	 * @param arraylist
	 *            of scores
	 */
	public static void outputScore(ArrayList<Score> scores) {
		if (scores.size() == 0) {
			System.out.println("No score saved");
		} else {
			int size;
			if (scores.size() > 9) {
				size = 10;
			} else {
				size = scores.size();
			}
			for (int i = 0; i < size; i++) {
				System.out.println(i + 1 + ". " + "Name: "
						+ scores.get(i).getName() + " Score: "
						+ scores.get(i).getScore());
			}
		}
	}
}
