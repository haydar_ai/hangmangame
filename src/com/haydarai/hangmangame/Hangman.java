package com.haydarai.hangmangame;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import java.util.Scanner;

import com.haydarai.hangmangame.helper.HangmanHelper;
import com.haydarai.hangmangame.helper.ScoreHelper;
import com.haydarai.hangmangame.helper.StringHelper;
import com.haydarai.hangmangame.model.Score;

/**
 * Hangman.java Purpose: Main class for HangmanGame
 * 
 * @author Haydar Ali Ismail
 * @version 1.0
 */
public class Hangman {

	private static Scanner sc = new Scanner(System.in);

	private static String[] dictionary = { "abbey", "abruptly", "affix",
			"askew", "axiom", "azure", "bagpipes", "bandwagon", "banjo",
			"bayou", "bikini", "blitz", "bookworm", "boxcar", "boxful",
			"buckaroo", "buffalo", "buffoon", "cobweb", "croquet", "daiquiri",
			"disavow", "duplex", "dwarves", "equip", "exodus", "fishook",
			"fixable", "foxglove", "galaxy", "galvanize", "gazebo", "gizmo",
			"glowworm", "guffaw", "haiku", "haphazard", "hyphen", "icebox",
			"injury", "ivory", "ivy", "jaundice", "jawbreaker", "jaywalk",
			"jazzy", "jigsaw", "jiujitsu", "jockey", "jovial", "joyful",
			"juicy", "jumbo" };
	private static String answer = new String();
	private static String guess = new String();
	private static String badGuess = new String();
	private static boolean won = false;
	private static boolean lose = false;
	private static ArrayList<Score> scores = new ArrayList<Score>();

	public static void main(String[] args) {
		initMenu();
	}

	/**
	 * Create menu interface for Hangman Game
	 */
	private static void initMenu() {
		System.out.println("H A N G M A N");
		System.out.println("--------------------");
		System.out.println("1. Play Game");
		System.out.println("2. How To Play");
		System.out.println("3. Scores");
		System.out.println("4. Exit");
		System.out.println("--------------------");
		System.out.println("Created by Haydar Ali Ismail 2014");
		System.out.print("Input: ");
		while (!sc.hasNext()) {
			System.out.print("Please input the correct option: ");
			sc.next();
		}
		switch (sc.next()) {
		case "1":
			initGame();
			break;
		case "2":
			initTutorial();
			break;
		case "3":
			initScores();
		case "4":
			System.exit(0);
			break;
		default:
			System.out.println("Please input the correct option");
			initMenu();
			break;
		}
	}

	/**
	 * Method for initiating the game
	 */
	private static void initGame() {
		final String WORD = getString();
		answer = StringHelper.convertStringToUnderscore(WORD);
		playGame(WORD);
		if (won) {
			gameWon();
		} else {
			gameLose();
		}
	}

	/**
	 * Method for initiating the tutorial screen
	 */
	private static void initTutorial() {
		System.out
				.println("This a single player hangman game. Player will be given an unknown");
		System.out
				.println("word which will be the keyword to be saved from sentece to death.");
		System.out
				.println("Player then will guess the word by guessing the letter one by one.");
		System.out
				.println("If player guess correctly then the player life will be preserved.");
		System.out
				.println("But if the player guess it wrong then the death sentence will go on.");
		System.out
				.println("If player can guess correctly before the death sentence finished,");
		System.out.println("then the player can live.");
		System.out.println();
		System.out.println("May the odds be in your favor~");
		System.out.println("--------------------");
		System.out.println("1. Back To Menu");
		System.out.println("--------------------");
		System.out.print("Input: ");
		while (!sc.hasNext()) {
			System.out.print("Please input the correct option: ");
			sc.next();
		}
		switch (sc.next()) {
		case "1":
			initMenu();
			break;
		default:
			System.out.println("Please input the correct option");
			initTutorial();
			break;
		}
	}

	/**
	 * Method for initiating the tutorial screen
	 */
	private static void initScores() {
		ScoreHelper.outputScore(scores);
		System.out.println("--------------------");
		System.out.println("1. Back To Menu");
		System.out.println("--------------------");
		System.out.print("Input: ");
		while (!sc.hasNext()) {
			System.out.print("Please input the correct option: ");
			sc.next();
		}
		switch (sc.next()) {
		case "1":
			initMenu();
			break;
		default:
			System.out.println("Please input the correct option");
			initScores();
			break;
		}
	}

	/**
	 * Method for getting a word for playing HangmanGame
	 * 
	 * @return A String from dictionary
	 */
	private static String getString() {
		Random r = new Random();
		return dictionary[r.nextInt(dictionary.length)];
	}

	/**
	 * Method to only take AlphabetCharacter
	 * 
	 * @return A String from user input
	 */
	private static String getInputAlphabetCharacter() {
		while (!sc.hasNext("[A-Za-z]+")) {
			System.out.println();
			System.out.println("Please insert a valid alphabet letter");
			System.out.println();
			System.out.print("Write your guess: ");
			sc.next();
		}
		return sc.next().substring(0, 1).toUpperCase();
	}

	/**
	 * Method for initiating the Game Interface
	 */
	private static void createGameInterface() {
		// Add space after each letter
		System.out.println("Word: " + answer.replaceAll(".(?!$)", "$0 "));
		System.out.println();
		System.out.println("Bad Guess: " + badGuess);
		System.out.println();
		HangmanHelper.printHangman(badGuess.length());
		System.out.println();
		System.out.print("Write your guess: ");
	}

	/**
	 * Rules for HangmanGame and how the program handle the input
	 */
	private static void playGame(String word) {
		while (!(word.equalsIgnoreCase(answer)) && !(won) && !(lose)) {
			createGameInterface();
			String newGuess = getInputAlphabetCharacter();

			// Check whether the newest guess already being guessed before
			if (guess.contains(newGuess)) {
				System.out.println("You have guessed using this word!");
				System.out.println();
				continue;
			} else {
				// Append the newest guess if it haven't guessed yet
				guess += newGuess;
			}

			// Indicator whether the newest guess is in the word or not
			boolean newGuessCorrect = false;
			for (int i = 0; i < word.length(); i++) {
				String currentLetter = word.substring(i, i + 1);
				if (newGuess.equalsIgnoreCase(currentLetter)) {
					// Change the corresponding index that has been guessed with
					// the real letter
					answer = answer.substring(0, i) + newGuess
							+ answer.substring(i + 1, word.length());
					newGuessCorrect = true;
				}
			}
			if (!newGuessCorrect) {
				// If the guess is not in the word, then record it in bad guess
				badGuess += newGuess;
			}

			// If player have bad guess 7 time
			if (badGuess.length() > 6) {
				lose = true;
			}

			// If player have get all the word correct
			if (word.equalsIgnoreCase(answer)) {
				won = true;
			}
		}
	}

	/**
	 * Indicating the player win
	 */
	private static void gameWon() {
		System.out.println("Word: " + answer.replaceAll(".(?!$)", "$0 "));
		System.out.println("Congratulations, you are saved!");
		ScoreHelper.inputScore(scores, (7 - badGuess.length()));
		playAgain();
	}

	/**
	 * Indicating the player lose with datetime information
	 */
	private static void gameLose() {
		HangmanHelper.printHangman(badGuess.length());
		System.out.println("You are dead by hanging!");
		System.out.println("R.I.P: Hangman");
		System.out.println(new SimpleDateFormat("dd-MM-yyyy HH:mm")
				.format(Calendar.getInstance().getTime()));
		playAgain();
	}

	/**
	 * Ask player to play again
	 */
	private static void playAgain() {
		System.out.print("Play again? (Y/N): ");
		switch (getInputAlphabetCharacter()) {
		case "Y":
			resetVariables();
			initGame();
			break;
		case "N":
			resetVariables();
			initMenu();
		default:
			break;
		}
	}

	/**
	 * Reset all variables so player can play again
	 */
	private static void resetVariables() {
		won = false;
		lose = false;
		guess = new String();
		badGuess = new String();
	}
}
