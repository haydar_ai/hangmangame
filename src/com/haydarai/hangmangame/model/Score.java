package com.haydarai.hangmangame.model;

/**
 * Score.java Purpose: Model for Score
 * 
 * @author Haydar Ali Ismail
 * @version 1.0
 */
public class Score {

	private String name;
	private int score;

	public Score(String name, int score) {
		this.name = name;
		this.score = score;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

}
